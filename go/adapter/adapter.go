package adapter

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"time"

	pb "gitlab.com/WST-Mike/joost-sdk/go/adapter/pb"
	"google.golang.org/grpc"
)

type IAdapter interface {
	SetServer(srv *adapterServer)
	OnActivate() error
	OnDeactivate() error
}

type Adapter struct {
	srv *adapterServer
}

func (a *Adapter) SetServer(srv *adapterServer) {
	a.srv = srv
}

func (a *Adapter) WriteEvent(ev *Event) error {
	a.srv.WriteEvent(ev)
	return nil
}

func AdapterMain(adp IAdapter) {
	log.Println("Time to start adapter :>")
	srvPort := 0
	dev := flag.Bool("dev", false, "Dev mode")
	port := flag.Int("port", 0, "port")
	flag.Parse()

	srv := newAdapterServer(adp)
	adp.SetServer(srv)

	if *dev {
		log.Println("Start dev mode?")
		//No need to start the GRPC server really :>
		srv.OnActivate(context.Background(), &pb.OnActivateRequest{})
		devSrv := &adapterStreamEventsDevServer{}
		log.Println("Called stream events")
		go srv.StreamEvents(&pb.StreamEventsRequest{}, devSrv)
		time.Sleep(time.Second * 40000)
		srv.OnDeactivate(context.Background(), &pb.OnDeactivateRequest{})
		os.Exit(1)
	}

	if *port == 0 {
		log.Fatal("Invalid port provided")
	} else {
		srvPort = *port
	}

	lis, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", srvPort))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	grpcServer := grpc.NewServer()
	pb.RegisterAdapterServer(grpcServer, srv)
	grpcServer.Serve(lis)
}
