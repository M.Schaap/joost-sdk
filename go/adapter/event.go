package adapter

import (
	"time"
)

type Event struct {
	SourceTime time.Time
	DataPoint  string
	EventType  string
	Metadata   map[string]interface{}
	Payload    map[string]interface{}
}
