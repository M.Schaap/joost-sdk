package adapter

import (
	"context"
	"encoding/json"
	"log"
	"time"

	pb "gitlab.com/WST-Mike/joost-sdk/go/adapter/pb"
	"google.golang.org/grpc"
)

type adapterServer struct {
	impl IAdapter
	msg  chan *Event
}

func newAdapterServer(impl IAdapter) *adapterServer {
	return &adapterServer{
		impl: impl,
		msg:  make(chan *Event, 1000),
	}
}

func (srv *adapterServer) OnActivate(ctx context.Context, req *pb.OnActivateRequest) (*pb.OnActivateResponse, error) {
	srv.impl.OnActivate()
	return &pb.OnActivateResponse{}, nil
}

func (srv *adapterServer) StreamEvents(req *pb.StreamEventsRequest, stream pb.Adapter_StreamEventsServer) error {
	log.Println("StreamEvents requested")
	for {
		m := <-srv.msg
		var err error
		metadata := []byte{}
		if m.Metadata != nil {
			metadata, err = json.Marshal(m.Metadata)
			if err != nil {
				log.Println("error marshalling metadata", err)
				return err
			}
		}

		payload := []byte{}
		if m.Payload != nil {
			payload, err = json.Marshal(m.Payload)
			if err != nil {
				log.Println("error marshalling payload", err)
				return err
			}
		}

		n := &pb.StreamEventsResponse{
			SourceTime: uint64(m.SourceTime.Unix()),
			DataPoint:  m.DataPoint,
			EventType:  m.EventType,
			Metadata:   metadata,
			Payload:    payload,
		}
		if err := stream.Send(n); err != nil {
			log.Printf("Stream connection failed: %v", err)
			return nil
		}
		//log.Printf("Event sent: %+v", m)
	}
}

func (srv *adapterServer) OnDeactivate(ctx context.Context, req *pb.OnDeactivateRequest) (*pb.OnDeactivateResponse, error) {
	srv.impl.OnDeactivate()
	return &pb.OnDeactivateResponse{}, nil
}

func (srv *adapterServer) WriteEvent(ev *Event) {
	//log.Println("Wirting event to chan: ", ev)
	select {
	case srv.msg <- ev:
	case <-time.After(5 * time.Second):
		log.Println("Timeout")
	}
}

//Dev

type adapterStreamEventsDevServer struct {
	grpc.ServerStream
}

func (x *adapterStreamEventsDevServer) Send(m *pb.StreamEventsResponse) error {
	//log.Println("Sending message: ", m)
	return nil
}
