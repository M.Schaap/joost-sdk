package adapter

import (
	"context"
	"encoding/json"
	"log"
	"os/exec"
	"time"

	pb "gitlab.com/WST-Mike/joost-sdk/go/adapter/pb"
)

type Client struct {
	Cmd        *exec.Cmd
	GrpcClient pb.AdapterClient
}

func (c *Client) OnActivate() error {
	_, err := c.GrpcClient.OnActivate(context.Background(), &pb.OnActivateRequest{})
	return err
}

func (c *Client) StreamEvents(eventChan chan *Event) error {
	stream, err := c.GrpcClient.StreamEvents(context.Background(), &pb.StreamEventsRequest{})
	if err != nil {
		return err
	}

	for {
		r, err := stream.Recv()
		if err != nil {
			log.Println("Dit we stop>?", err)
			return err
		}

		metadata := make(map[string]interface{})
		payload := make(map[string]interface{})

		if r.Metadata != nil {
			err = json.Unmarshal(r.Metadata, &metadata)
			if err != nil {
				return err
			}
		}

		if r.Payload != nil {

			err = json.Unmarshal(r.Payload, &payload)
			if err != nil {
				return err
			}
		}

		eventChan <- &Event{
			SourceTime: time.Unix(int64(r.SourceTime), 0),
			DataPoint:  r.DataPoint,
			EventType:  r.EventType,
			Metadata:   metadata,
			Payload:    payload,
		}
	}
	return nil
}

func (c *Client) OnDeactivate() error {
	_, err := c.GrpcClient.OnDeactivate(context.Background(), &pb.OnDeactivateRequest{})
	return err
}
