gen:
	protoc -I . adapter.proto --go_out=plugins=grpc:go/adapter/pb
gen-python:
	python3 -m grpc_tools.protoc -I. --python_out=./python --grpc_python_out=./python adapter.proto